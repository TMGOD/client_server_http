#!/usr/bin/env -S python3
import sys
import requests
import asyncio
import threading
from time import perf_counter, sleep

def add_threading(n, thr):
    if thr == 2 or thr == 6:
        match thr:
            case 2:
                print("%d request, %d threads" %(n, thr))
                for i in range(n):
                    th1 = threading.Thread(target=get_request)
                    th2 = threading.Thread(target=get_request)

                    th1.start()
                    th2.start()

                    th1.join()
                    th2.join()
            case 6:
                print("%d request, %d threads" %(n, thr))
                for i in range(n):
                    th0 = threading.Thread(target=get_request)
                    th1 = threading.Thread(target=get_request)
                    th2 = threading.Thread(target=get_request)
                    th3 = threading.Thread(target=get_request)
                    th4 = threading.Thread(target=get_request)
                    th5 = threading.Thread(target=get_request)

                    th0.start()
                    th1.start()
                    th2.start()
                    th3.start()
                    th4.start()
                    th5.start()

                    th0.join()
                    th1.join()
                    th2.join()
                    th3.join()
                    th4.join()
                    th5.join()
    else:
        print("1 request, 1 thread")
        th0 = threading.Thread(target=get_request)
        th0.start()

    return 0

def get_request():
    try:
        res = requests.get(addr_server)
        start = perf_counter()
        if res.status_code == 200:
            end = perf_counter()
            print("Connection established with the server")
            print("In %s sec" % "{:.6f}".format(end-start))
        elif res.status_code == 301:
            end = perf_counter()
            print("Error 301: Moved Permanently")
        elif res.status_code == 401:
            end = perf_counter()
            print("Error 401: Unauthorized")
        elif res.status_code == 404:
            end = perf_counter()
            print("Error 404: Not found")
    except requests.exceptions.ConnectionError:
        print("Failed to establish a new connection: Name or service not known")

    return 0

def switch(opt=0, n=None, thr=None):
    if opt in range(0, 3):
        match opt:
            case 1:
                get_request()
            case 2:
                add_threading(n,thr)

    return 0

async def main():
    global verbose
    global addr_server

    start = perf_counter()
    try:
        addr_server = str(sys.argv[4])
        print(addr_server)
    except IndexError:
        addr_server = 'https://google.com/'
        print("Default server : %s" %(addr_server))

    try:
        opt = int(sys.argv[1])
        n = int(sys.argv[2])
        thr = int(sys.argv[3])
        switch(opt, n, thr)
    except IndexError:
        switch(1)
    except ValueError:
        addr_server = sys.argv[1]
        switch(1)
    end = perf_counter()
    print("Total process :")
    print("In %s sec" % "{:.6f}".format(end-start))

    return 0

if __name__ == '__main__':
    asyncio.run(main())
